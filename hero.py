import sys
import json
import os
from urllib.request import urlopen
from urllib.parse import urlencode

API_KEY = os.environ.get('API_KEY')
APP_ID = 570
API_BASE = 'http://api.steampowered.com/IEconDOTA2_{0}'.format(APP_ID)


def get_heroes(outfile):
    if outfile is None:
        raise TypeError(
            'Missing one required positional argument: \'outfile\'')
    data = {
        'key': API_KEY
    }
    params = urlencode(data)
    url = '{basepath}/GetHeroes/v1?{query}'.format(
        basepath=API_BASE, query=params)
    with urlopen(url) as f:
        response_data = json.loads(f.read().decode('utf-8'))

    for item in response_data['result']['heroes']:
        item['name'] = item['name'].replace('_', ' ')[14:]

    with open(outfile, 'w') as fd:
        return json.dump(response_data, fd, indent=2)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        get_heroes(sys.argv[1])
    else:
        sys.exit('Required argument "filename" not supplied')

